// https://api.weatherbit.io/v2.0/current
import React, {useState, useEffect} from 'react'
import axios from 'axios'

const Current = () => {

    const [data, setData] = useState([])

    useEffect(() => {
      axios
      .get('https://api.weatherbit.io/v2.0/current?&postal_code=18603&country=US&units=I&key=28ffc74c32284e00bc5ba8a05bdd8221')
      .then((res) => {
          setData(res.data.data)
          console.log("current", res.data.data)})
      .catch((err) => console.error(err))
    }, [])
  
    if (!data) {
      return <p>Current Weather unavailable at this time</p>
    }

    return (
        <>
        <h2>Current Weather for Berwick, PA</h2>
        {data.map((current) => {
            return (
            <p>{current.app_temp}</p>
            )
        })}
        </>
    )
}

export default Current