// http://api.weatherbit.io/v2.0/forecast/hourly?&postal_code=18603&country=US&units=I&hours=6&key=28ffc74c32284e00bc5ba8a05bdd8221

import React, {useState, useEffect} from 'react'
import axios from 'axios'

const Current = () => {

    const [data, setData] = useState([])

    useEffect(() => {
      axios
      .get('http://api.weatherbit.io/v2.0/forecast/hourly?&postal_code=18603&country=US&units=I&hours=48&key=28ffc74c32284e00bc5ba8a05bdd8221')
      .then((res) => {
          setData(res.data.data[40])
          console.log("forecast", res.data.data[40])})
      .catch((err) => console.error(err))
    }, [])
  
    if (!data) {
      return <p>Current Weather unavailable at this time</p>
    }

    return (
        <>
        <h2>Current Weather for Berwick, PA</h2>
        {data.map((forecast) => {
            return (
                <>
            <p>{forecast.app_temp}</p>
            <p>{forecast.weather.description}</p>
            </>
            )
        })}
        </>
    )
}

export default Current