import React, {useEffect, useState} from 'react'
import axios from 'axios'

const Alerts = () => {

    const [data, setData] = useState([])

  useEffect(() => {
    axios
    .get('https://api.weatherbit.io/v2.0/alerts?&postal_code=18603&country=US&key=28ffc74c32284e00bc5ba8a05bdd8221')
    .then((res) => {
        setData(res.data.alerts)
        console.log("alerts", res.data.alerts)})
    .catch((err) => console.error(err))
  }, [])

  if (!data) {
    return <p>No Alerts At this time</p>
  }

    return (
        <>
        <h2>Current Alerts for Berwick, PA</h2>
        {data.map((alert) => {
            return (
                <p>{alert.description}</p>
            )
        })}
        </>
    )
}
export default Alerts