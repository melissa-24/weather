import React from 'react'

import './App.css'
import Current from './components/current'
// import Forecast from './components/forecast'
import Alerts from './components/alerts'

const App = () => {

  return (
    <div className="App">
       <h1>Weather</h1>
       <Current />
       {/* <Forecast /> */}
       <Alerts />
    </div>
  );
};

export default App;